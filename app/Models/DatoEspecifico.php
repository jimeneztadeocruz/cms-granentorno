<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DatoEspecifico extends Model
{
    protected $table= "datos_especificos";

    protected $fillable = ["precio","direccion","colonia","numero_telefonico","email","descripcion","longitud","latitud","estado","porcentaje"];

    protected $dates = ["created_at","updated_at"];
}
