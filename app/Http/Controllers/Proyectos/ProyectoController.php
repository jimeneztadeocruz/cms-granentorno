<?php

namespace App\Http\Controllers\Proyectos;

use App\Http\Controllers\Controller;
use App\Models\Caracteristica;
use App\Models\DatoEspecifico;
use App\models\Proyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;

class ProyectoController extends Controller
{
    public function createProject(Request $request)
    {

        $datos_especificos = DatoEspecifico::create($request->datos_especificos);
        if (isset($datos_especificos->id)) {
            $pdf_r = str_replace('data:application/pdf;base64','',$request->datos_especificos['pdf']);
            $pdf_r = str_replace(' ','+',$pdf_r);
            if ($datos_especificos->pdf == null) {
                $pdf_path = time() . "folleto.pdf";
                Storage::disk('recursos')->put($pdf_path, $pdf_r);
                $dato_especifico = DatoEspecifico::where('id', '=', $datos_especificos->id)->first();
                $dato_especifico->pdf = $pdf_path;
                $dato_especifico->save();
            }
        }
        $proyecto = new Proyecto();
        foreach ($request->proyecto as $nombre=>$project) {
           if($nombre == 'tipo'){
               $proyecto->tipo = $project;
           }else{
               $proyecto->estado = $project;
           }
        }
        $proyecto->datos_especificos_id = $datos_especificos->id;
        $proyecto->save();

        $caracteristicas = $request->caracteristicas_principales;
        foreach ($caracteristicas as $nombre => $cantidad) {
            $caracteristica_principal = new Caracteristica();
            $caracteristica_principal->nombre = $nombre;
            $caracteristica_principal->cantidad = $cantidad;
            $caracteristica_principal->tipo = "principal";
            $caracteristica_principal->proyecto_id = $proyecto->id;
            $caracteristica_principal->save();
        }

        return "listo";
    }
}
