<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatoEspecificosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_especificos', function (Blueprint $table) {
            $table->increments('id');
            $table->float('precio');
            $table->string('direccion');
            $table->string('colonia');
            $table->string('numero_telefonico');
            $table->string('email');
            $table->string('descripcion');
            $table->string('latitud');
            $table->string('longitud');
            $table->string('pdf');
            $table->string('estado');
            $table->string('porcentaje');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dato_especificos');
    }
}
